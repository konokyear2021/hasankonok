import logo from './logo.svg';
import './App.css';
import NavBar from './components/navBar';
import Bannar from './components/Bannar';
import Skill from './components/Skills';
import Project from './components/Projects'
import Contacat from './components/Contact'
import { Footer } from './components/Footer';
import 'bootstrap/dist/css/bootstrap.min.css'

function App() {
  return (
  <>
   <div className="navBar">
    <NavBar/>
    <Bannar/>
    <Skill/>
    <Project/>
    <Contacat/>
    <Footer/>
   </div>
  </>
  );
}

export default App;
