import { useEffect, useState } from "react"
import { Navbar,Container,Nav } from "react-bootstrap"
import logo from '../assets/img/konok.svg'
import NavIcon1 from '../assets/img/nav-icon1.svg'
import NavIcon2 from '../assets/img/nav-icon2.svg'
import NavIcon3 from '../assets/img/nav-icon3.svg'



const NavBar = () => {

    const [activLink, setActiveLink] = useState('home');
    const [scrolled, setScrolled] = useState(false);
    useEffect(() => {
        const onScroll = () => {
            if(window.scrollY > 50 ) {
                setScrolled(true)
            }else {
                setScrolled(false);
            }
        }
        window.addEventListener("scroll", onScroll);

        return () => window.removeEventListener("scroll",onScroll)
    },[]);

    const  onUpdateActiveLink = (value) => {
        setActiveLink(value);
    }

  return (
    <>
    <Navbar expand="lg" className={scrolled ? "scrolled" : ''}>
      <Container>
        <Navbar.Brand href="#home">
            <img src={logo} alt="logo"/>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            {/* <span className="nav-Toggle-icon"></span>
        </Navbar.Toggle> */}
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="#home" className={activLink === 'home' ? "active navbar-link" : 'navbar-link' } 
            onClick={ () =>  onUpdateActiveLink=('home') }>Home</Nav.Link>
            <Nav.Link href="#Skill" className={activLink === 'Skill' ? "active navbar-link" : 'navbar-link' } 
            onClick={ () =>  onUpdateActiveLink=('Skill') }>Skill</Nav.Link>
            <Nav.Link href="#Project" className={activLink === 'Project' ? "active navbar-link" : 'navbar-link' } 
            onClick={ () =>  onUpdateActiveLink=('Project') }>Project</Nav.Link>
          </Nav>
          <span className="navbar-text">
            <div className="social-icon">
                <a href=""><img src={NavIcon1} alt="social"/></a>
                <a href=""><img src={NavIcon2} alt="social"/></a>
                <a href=""><img src={NavIcon3} alt="social"/></a>
            </div>
            <button className="vvd" onClick={ () => {
                console.log('contact us')
            }}> <span>Let's contact</span> </button>
          </span>
        </Navbar.Collapse>
      </Container>
    </Navbar>
    </>
  )
}

export default NavBar;